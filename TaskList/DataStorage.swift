//
//  DataStorage.swift
//  TaskList
//
//  Created by Дмитрий on 04.08.2023.
//
import CoreData

final class DataStorage {
    
    static let shared = DataStorage()
    
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TaskList")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    let viewContext: NSManagedObjectContext
    
    var taskList: [Task] = []
    
    private init() {
        viewContext = persistentContainer.viewContext
        taskList = fetchData()
    }
    
    func saveContext() {
        if viewContext.hasChanges {
            do {
                try viewContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func fetchData() -> [Task] {
        let fetchRequest = Task.fetchRequest()
        
        do {
            let taskList = try viewContext.fetch(fetchRequest)
            return taskList
        } catch {
            print(error)
            return []
        }
    }
    
    func save(_ taskName: String) {
        let task = Task(context: viewContext)
        task.title = taskName
        taskList.append(task)
        changes()
    }
    
    func edit(_ taskName: String, indexPath: IndexPath) {
        let taskToUpdate = taskList[indexPath.row]
            taskToUpdate.title = taskName
        changes()
    }
    
    func remove(indexPath: IndexPath) {
        taskList.remove(at: indexPath.row)
        changes()
    }
    
    private func changes() {
        if viewContext.hasChanges {
            do {
                try viewContext.save()
            } catch {
                print(error)
            }
        }
    }
}

