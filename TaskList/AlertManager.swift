//
//  AlertManager.swift
//  TaskList
//
//  Created by Дмитрий on 09.08.2023.
//

import UIKit

enum AlertType: Equatable {
    case edit
    case add
}

final class AlertManager {
    
    static let shared = AlertManager()
    
    private init() {}
    
    func alertController(for type: AlertType, currentValue: String? = nil, completion: @escaping (String?) -> Void) -> UIAlertController {
        let title: String = type == .add ? "Add Task" : "Edit Task"
        let alert = UIAlertController(title: title, message: "Please enter your value.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Save", style: .default) { _ in
            guard let task = alert.textFields?.first?.text, !task.isEmpty else { return }
            completion(task)
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive))
        
        alert.addTextField { textField in
            textField.placeholder = "Enter task"
            if let currentValue = currentValue {
                textField.text = currentValue
            }
        }
        
        return alert
    }
    
    func presentAlert(_ type: AlertType, currentValue: String? = nil, in viewController: UIViewController, completion: @escaping (String?) -> Void) {
        let alert = alertController(for: type, currentValue: currentValue, completion: completion)
        viewController.present(alert, animated: true, completion: nil)
    }
}
